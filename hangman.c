#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void printHangman(int incorrectAnswers) {
    printf("\n");
    switch (incorrectAnswers) {
        case 0:
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            break;
        case 1:
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf("\n");
            printf(" _________\n");
            break;
        case 2:
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 3:
            printf("__________\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 4:
            printf("__________\n");
            printf("| /\n");
            printf("|/\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 5:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 6:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/      O\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 7:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/      O\n");
            printf("|       |\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 8:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/      O\n");
            printf("|       |\n");
            printf("|      /\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 9:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/      O\n");
            printf("|       |\n");
            printf("|      / \\\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 10:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/      O\n");
            printf("|      -|\n");
            printf("|      / \\\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        case 11:
            printf("__________\n");
            printf("| /     |\n");
            printf("|/      O\n");
            printf("|      -|-\n");
            printf("|      / \\\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|\n");
            printf("|_________\n");
            break;
        default:
            printf("Something has gone horribly wrong!\n");
            printf("You've been arrested for torture!\n");
            break;
    }

    printf("\n");
}


void findWordOnLine(FILE* dictionary, char* currentWord, int wordOnLine) {

    char charRead;
    int currentLine = wordOnLine;
    int positionInWord = 0;

    while (currentLine >= 0) {
        charRead = fgetc(dictionary);
        if (charRead == '\n') {
            currentLine--;
            currentWord[positionInWord] = '\0';
            positionInWord = 0;
        }
        else {
            currentWord[positionInWord] = tolower(charRead);
            positionInWord++;
        }
    }

}

void printWordStatus (int printWordLength, char* word, int* gotWhichLetters) {

    int i;

    for (i = 0; word[i] != '\0'; i++) {
        if (gotWhichLetters[i] == 1) {
            printf("%c ", word[i]);
        }
        else {
            printf("_ ");
        }
    }
    printf("\n\n");
}

void printGuessedLetters (char* guessedLetters) {

    int i;

    printf("So far you have guessed:");
    for (i = 0; i < 26; i ++) {
        if (guessedLetters[i] == '\0') {
            break;
        }
        else {
            printf(" %c", guessedLetters[i]);
        }
    }
    printf("\n");
}

int main (void) {

    // Open dictionary for usage
    FILE* dictionary = fopen("/usr/share/dict/cracklib-small", "r");

    if (dictionary == NULL) {
        printf("Failed to open dictionary!\n");
        exit(1);
    }

    // Find number of lines and max word count
    char* currentWord;
    int lineCount = 0, currentLine = 0;
    char charRead = 'a';
    int maxLineLength = 0, currentLineLength = 0;

    while (charRead != EOF) {
        charRead = fgetc(dictionary);
        currentLineLength++;//Even if \n for \0 in word later
        if (charRead == '\n') {
            lineCount++;
            if ( currentLineLength > maxLineLength) {
                maxLineLength = currentLineLength;
            }
            currentLineLength = 0;
        }
    }

    //Need to warn if dictionary is too big

    currentWord = calloc(maxLineLength, sizeof(char));
    rewind(dictionary);

    srand(time(NULL));
    currentLine = rand() % lineCount;

    int foundWord = 0;
    int wordLength;

    while (foundWord != 1) {
        findWordOnLine(dictionary, currentWord, currentLine);
        foundWord = 1;
        for (wordLength = 0; wordLength < maxLineLength; wordLength++) {
            if (currentWord[wordLength] == '\0') {
                break;
            }
            else if (isalpha(currentWord[wordLength]) == 0) {
                foundWord = 0;
            }
        }
    }

    //printf("Word is %s\n", currentWord);

    int i;
    int* gotWhichLetters = calloc(maxLineLength, sizeof(int));
    char* inputtedWorL = calloc(maxLineLength, sizeof(char));
    char guessedLetters[26] = {'\0'};
    int numOfGuesses = 0;
    int alreadyGuessed = 0;
    int livesUsed = 0;
    int foundLetter = 0;
    int gotAllLetters = 0;

    while (livesUsed < 11) {
        printf("Type the letter or word that you want to guess\n");
        scanf("%s", inputtedWorL);
        if (inputtedWorL[1] == '\0') {
            for (i = 0; i < 26; i++) {
                if (guessedLetters[i] == inputtedWorL[0]) {
                    alreadyGuessed = 1;
                    break;
                }
            }
            if (alreadyGuessed) {
                printf("You have already guessed %c!\n", inputtedWorL[0]);
                alreadyGuessed = 0;
            }
            else {
                for (i = 0; currentWord[i] != '\0'; i++) {
                    if (currentWord[i] == inputtedWorL[0]) {
                        gotWhichLetters[i] = 1;
                        foundLetter = 1;
                    }
                }
                if (foundLetter) {
                    printf("You found %c in the word!\n", inputtedWorL[0]);
                }
                else {
                    printf("%c does not exist in the word!\n", inputtedWorL[0]);
                    livesUsed++;
                }
                foundLetter = 0;
                guessedLetters[numOfGuesses] = inputtedWorL[0];
                numOfGuesses++;
            }
            for (i = 0; currentWord[i] != '\0'; i++) {
                if (gotWhichLetters[i] != 1) {
                    gotAllLetters = 0;
                    break;
                }
                else {
                    gotAllLetters = 1;
                }
            }
            if (gotAllLetters) {
                break;
            }
        }
        else {
            if (strcmp(inputtedWorL, currentWord) != 0) {
                printf("%s is incorrect!\n\n", inputtedWorL);
                livesUsed++;
            }
            else {
                break;
            }
        }
        printWordStatus(wordLength, currentWord, gotWhichLetters);
        printGuessedLetters(guessedLetters);
        printHangman(livesUsed);
    }

    if (livesUsed < 11) {
        printf("You won!\nThe word was %s\n", currentWord);
    }
    else {
        printf("You ran out of lives!\nThe word was %s\n", currentWord);
    }

    return 0;
}
